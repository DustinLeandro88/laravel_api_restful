<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentsTable extends Migration
{
    public function up()
    {
        Schema::create('rents', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('rental_date');
            $table->dateTime('return_date');
            $table->unsignedInteger('client_id');
            $table->unsignedInteger('movie_id');
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('movie_id')->references('id')->on('movies');
        });
    }

    public function down()
    {
        Schema::dropIfExists('rents');
    }
}
