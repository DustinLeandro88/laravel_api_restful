<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(ClientsSeeder::class);
        $this->call(MoviesSeeder::class);
        $this->call(PhonesSeeder::class);
        $this->call(RentsSeeder::class);
        $this->call(DocumentsSeeder::class);
    }

}

