<?php

use Faker\Generator as Faker;
use App\Models\Phone;

$factory->define(Phone::class, function (Faker $faker) {
    return [
        'number' => $faker->numerify($string = '+## ## #####-####'),
        'client_id' => $faker->randomElement($array = array (1, 2, 3, 4, 5)),
    ];
});
