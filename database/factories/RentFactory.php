<?php

use Faker\Generator as Faker;
use App\Models\Rent;

$factory->define(Rent::class, function (Faker $faker) {
    return [
        'rental_date' => $faker->dateTime(),
        'return_date' => $faker->dateTime(),
        'client_id' => $faker->randomElement($array = array (1, 2, 3, 4, 5)),
        'movie_id' => $faker->randomElement($array = array (1, 2, 3, 4, 5, 6, 7, 8, 9, 10)),
    ];
});
