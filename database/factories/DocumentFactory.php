<?php

use Faker\Generator as Faker;
use App\Models\Document;

$factory->define(Document::class, function (Faker $faker) {
    return [
        'cpf_cnpj' => $faker->unique()->numerify($string = '###########'),
        'client_id' => $faker->unique()->randomElement($array = array (1, 2, 3, 4, 5)),
    ];
});
