<?php

use Faker\Generator as Faker;
use App\Models\Movie;

$factory->define(Movie::class, function (Faker $faker) {
    return [
        'title' => $faker->jobTitle,
        'cover' => $faker->image('public/storage/images/movies', '150', '150', 'abstract', false),
        'rented' => $faker->randomElement($array = array ('Y', 'N')),
    ];
});
