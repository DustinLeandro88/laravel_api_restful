<?php

use Faker\Generator as Faker;
use App\Models\Client;

$factory->define(Client::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'image' => $faker->image('public/storage/images/clients', '150', '150', 'people', false),
    ];
});
