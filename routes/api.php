<?php
Route::post('login', 'Auth\AuthenticateController@authenticate');
Route::post('token-refresh', 'Auth\AuthenticateController@refreshToken');
Route::get('get-token', 'Auth\AuthenticateController@getAuthenticatedUser');

Route::group(['namespace' => 'Api', 'middleware' => 'auth:api'], function()
{
    Route::get('clientes/{id}/documento', 'ClientApiController@document');
    Route::get('clientes/{id}/telefone', 'ClientApiController@phone');
    Route::get('clientes/{id}/filmes-alugados', 'ClientApiController@renteds');
    Route::resource('clientes', 'ClientApiController');

    Route::get('documentos/{id}/cliente', 'DocumentApiController@client');
    Route::resource('documentos', 'DocumentApiController');

    Route::get('telefones/{id}/cliente', 'PhoneApiController@client');
    Route::resource('telefones', 'PhoneApiController');

    Route::get('filmes/{id}/cliente', 'MovieApiController@client');
    Route::resource('filmes', 'MovieApiController');
});