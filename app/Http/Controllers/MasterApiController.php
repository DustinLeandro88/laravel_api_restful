<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;

class MasterApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        $data = $this->model->all();
        return response()->json($data);
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->model->rules());

        $dataForm = $request->all();

        if ($request->hasFile($this->upload) && $request->file($this->upload)->isValid()) {
            $extension = $request->file($this->upload)->extension();

            $name = uniqid(date('His'));

            $nameFile = "$name.$extension";

            $upload = Image::make($dataForm[$this->upload])->resize($this->width, $this->height)
                ->save(storage_path("app/public/$this->path/$nameFile", 70));

            if (!$upload) {
                return response()->json(['error' => 'Não foi possível realizar upload da imagem'], 500);
            } else {
                $dataForm[$this->upload] = $nameFile;
            }
        }
        
        $data = $this->model->create($dataForm);

        return response()->json($data, 201);
    }

    public function show($id)
    {
        $data = $this->model->find($id);

        if ($data){
            return response()->json($data);
        } else {
            return response()->json(['error' => 'Cliente não encontrado'], 404);
        }
       
    }

    public function update(Request $request, $id)
    {
        if (!$data = $this->model->find($id)) {
            return response()->json(['error' => 'Cliente não encontrado!'], 404);
        }

        $this->validate($request, $this->model->rules());
        
        $dataForm = $request->all();

        if ($request->hasFile($this->upload) && $request->file($this->upload)->isValid()) {
            $file = $this->model->dataImage($id); 

            if ($file) {
                Storage::disk('public')->delete("$this->path/$file");
            }
            
            $extension = $request->file($this->upload)->extension();

            $name = uniqid(date('His'));

            $nameFile = "$name.$extension";

            $upload = Image::make($dataForm[$this->upload])->resize($this->width, $this->height)
                ->save(storage_path("app/public/$this->path/$nameFile", 70));

            if (!$upload) {
                return response()->json(['error' => 'Não foi possível realizar upload da imagem'], 500);
            } else {
                $dataForm[$this->upload] = $nameFile;
            }
        }
        
        $data->update($dataForm);

        return response()->json($data, 201);
    }

    public function destroy($id)
    {
        $data = $this->model->find($id);

        if ($data) {
            if ($data->image) {
                if (method_exists($this->model, 'dataImage')) {
                    Storage::disk('public')->delete("/$this->path/{$this->model->dataImage($id)}");
                }
            }

            $data->delete();
            return response()->json(['success' => 'Cliente removido!'], 200);
        } else {
            return response()->json(['error' => 'Cliente não encontrado!'], 404);
        }
    }
}
