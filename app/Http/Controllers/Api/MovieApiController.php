<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\MasterApiController;
use App\Models\Movie;

class MovieApiController extends MasterApiController
{
    protected $model;
    protected $path = 'images/movies';
    protected $upload = 'cover';
    protected $width = 800;
    protected $height = 520;
    protected $totalPage = 20;

    public function __construct(Movie $movie, Request $request)
    {
        $this->model = $movie;
        $this->request = $request;
    }

    public function index()
    {
        $data = $this->model->paginate($this->totalPage);
        return response()->json($data);
    }
}
