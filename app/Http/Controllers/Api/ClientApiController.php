<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\MasterApiController;
use App\Models\Client;

class ClientApiController extends MasterApiController
{
    protected $model;
    protected $path = 'images/clients';
    protected $upload = 'image';
    protected $width = 150;
    protected $height = 220;

    public function __construct(Client $client, Request $request)
    {
        $this->model = $client;
        $this->request = $request;
    }

    public function document($id)
    {
        $data = $this->model->with('document')->find($id);

        if ($data){
            return response()->json($data);
        } else {
            return response()->json(['error' => 'Documento não encontrado'], 404);
        }  
    }

    public function phone($id)
    {
        $data = $this->model->with('phone')->find($id);

        if ($data){
            return response()->json($data);
        } else {
            return response()->json(['error' => 'Telefone não encontrado'], 404);
        }
       
    }

    public function renteds($id)
    {
        $data = $this->model->with('movieRented')->find($id);

        if ($data){
            return response()->json($data);
        } else {
            return response()->json(['error' => 'Telefone não encontrado'], 404);
        }
       
    }
}
