<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\MasterApiController;
use App\Models\Phone;

class PhoneApiController extends MasterApiController
{
    protected $model;
    protected $path;
    protected $upload;

    public function __construct(Phone $phone, Request $request)
    {
        $this->model = $phone;
        $this->request = $request;
    }

    public function client($id)
    {
        $data = $this->model->with('client')->find($id);

        if ($data){
            return response()->json($data);
        } else {
            return response()->json(['error' => 'Cliente não encontrado'], 404);
        }
    }
}
