<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Client;

class Document extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'cpf_cnpj',
        'client_id'
    ];

    public function rules()
    {
        return 
        [
            'cpf_cnpj' => 'required|numeric|digits:11|unique:documents',
            'client_id' => 'required|numeric|unique:documents'
        ];
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id', 'id');
    }
}
