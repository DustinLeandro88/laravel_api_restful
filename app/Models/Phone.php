<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Client;

class Phone extends Model
{
    protected $fillable = [
        'number',
        'client_id'
    ];

    public function rules()
    {
        return 
        [
            'number' => 'required|string|between:8,20',
            'client_id' => 'required|numeric'
        ];
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id', 'id');
    }
}
