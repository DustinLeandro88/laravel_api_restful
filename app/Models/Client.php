<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Document;
use App\Models\Phone;
use App\Models\Movie;

class Client extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'image'
    ];

    public function rules()
    {
        return 
        [
            'name' => 'required|string|between:3,150',
            'image' => 'nullable|image'
        ];
    }

    public function dataImage($id)
    {
        $data = $this->find($id);
        return $data->image;
    }

    public function document()
    {
        return $this->hasOne(Document::class, 'client_id', 'id');
    }

    public function phone()
    {
        return $this->hasMany(Phone::class, 'client_id', 'id');
    }

    public function movieRented()
    {
        return $this->belongsToMany(Movie::class, 'rents');
    }
}
