<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

class Movie extends Model
{
    protected $fillable = [
        'title',
        'cover',
        'rented'
    ];

    public function rules()
    {
        return 
        [
            'title' => 'required|string|between:3,150',
            'cover' => 'nullable|image',
            'rented' => 'required', Rule::in(['Y', 'N'])
        ];
    }

    public function dataCover($id)
    {
        $data = $this->find($id);
        return $data->cover;
    }
}
